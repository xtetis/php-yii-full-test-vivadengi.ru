-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 13, 2017 at 11:08 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `ki_answers`
--

DROP TABLE IF EXISTS `ki_answers`;
CREATE TABLE `ki_answers` (
  `ki_answer_key` int(11) NOT NULL,
  `application_key` int(11) NOT NULL COMMENT 'Ключ заявки',
  `ki_question_key` int(11) NOT NULL COMMENT 'Ссылка на вопрос',
  `answer` varchar(200) NOT NULL DEFAULT '' COMMENT 'Ответ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список ответов';

--
-- Dumping data for table `ki_answers`
--

INSERT INTO `ki_answers` VALUES(1, 2, 1, '1');
INSERT INTO `ki_answers` VALUES(2, 2, 3, '1-10');
INSERT INTO `ki_answers` VALUES(3, 2, 4, '1');
INSERT INTO `ki_answers` VALUES(4, 2, 2, '1');
INSERT INTO `ki_answers` VALUES(5, 2, 5, '19.01.2017');
INSERT INTO `ki_answers` VALUES(6, 2, 6, '4444');
INSERT INTO `ki_answers` VALUES(7, 2, 7, 'disabled');
INSERT INTO `ki_answers` VALUES(8, 2, 9, '');
INSERT INTO `ki_answers` VALUES(9, 2, 10, '');
INSERT INTO `ki_answers` VALUES(10, 3, 1, '1');
INSERT INTO `ki_answers` VALUES(11, 3, 3, 'NULL');
INSERT INTO `ki_answers` VALUES(12, 3, 4, '4444444444');
INSERT INTO `ki_answers` VALUES(13, 3, 5, '');
INSERT INTO `ki_answers` VALUES(14, 3, 6, '44444444444');
INSERT INTO `ki_answers` VALUES(15, 3, 7, 'Доллары');
INSERT INTO `ki_answers` VALUES(16, 3, 8, 'пппппппппп');
INSERT INTO `ki_answers` VALUES(17, 3, 10, '');
INSERT INTO `ki_answers` VALUES(18, 4, 1, '');
INSERT INTO `ki_answers` VALUES(19, 4, 3, 'NULL');
INSERT INTO `ki_answers` VALUES(20, 4, 4, 'jhgfds');
INSERT INTO `ki_answers` VALUES(21, 4, 5, '');
INSERT INTO `ki_answers` VALUES(22, 4, 6, '55');
INSERT INTO `ki_answers` VALUES(23, 4, 7, 'Доллары');
INSERT INTO `ki_answers` VALUES(24, 4, 8, '4444');
INSERT INTO `ki_answers` VALUES(25, 4, 10, '');

-- --------------------------------------------------------

--
-- Table structure for table `ki_answers_formats`
--

DROP TABLE IF EXISTS `ki_answers_formats`;
CREATE TABLE `ki_answers_formats` (
  `ki_answer_format_key` int(11) NOT NULL,
  `answer_format_description` varchar(200) NOT NULL DEFAULT '' COMMENT 'Описание формата'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список форматов вопросов';

--
-- Dumping data for table `ki_answers_formats`
--

INSERT INTO `ki_answers_formats` VALUES(1, 'text');
INSERT INTO `ki_answers_formats` VALUES(2, 'select');
INSERT INTO `ki_answers_formats` VALUES(3, 'checkbox');
INSERT INTO `ki_answers_formats` VALUES(4, 'date');
INSERT INTO `ki_answers_formats` VALUES(5, 'int');

-- --------------------------------------------------------

--
-- Table structure for table `ki_applications`
--

DROP TABLE IF EXISTS `ki_applications`;
CREATE TABLE `ki_applications` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL DEFAULT '' COMMENT 'Имя',
  `last_name` varchar(200) NOT NULL DEFAULT '' COMMENT 'Фамилия',
  `phone` varchar(200) NOT NULL DEFAULT '' COMMENT 'Телефон',
  `questions` varchar(400) NOT NULL DEFAULT '' COMMENT 'Список сгенерированных вопросов',
  `use_credit` int(11) NOT NULL DEFAULT '0' COMMENT 'Пользовался ли ранее кредитом'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список анкетируемых';

--
-- Dumping data for table `ki_applications`
--

INSERT INTO `ki_applications` VALUES(2, 'Вахтанг', 'Хабибулин', '76531843', '2,3,4,5,6,7,9,10', 0);
INSERT INTO `ki_applications` VALUES(3, 'Иван', 'Катамаран', '222222222', '1,3,4,5,6,7,8,10', 0);
INSERT INTO `ki_applications` VALUES(4, 'ввввввввв', 'ввввввввввв', '2222222222', '1,3,4,5,6,7,8,10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ki_questions`
--

DROP TABLE IF EXISTS `ki_questions`;
CREATE TABLE `ki_questions` (
  `ki_question_key` int(11) NOT NULL,
  `question_text` varchar(200) NOT NULL DEFAULT '' COMMENT 'Вопрос',
  `question_group` int(11) NOT NULL DEFAULT '0' COMMENT 'Группа вопроса',
  `hint_text` varchar(200) NOT NULL DEFAULT '' COMMENT 'Хинт',
  `answer_format_key` int(11) NOT NULL DEFAULT '0' COMMENT 'Ссылка на формат ответа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список вопросов';

--
-- Dumping data for table `ki_questions`
--

INSERT INTO `ki_questions` VALUES(1, 'Пользовались ли кредитом ваши родственники', 1, 'Были ли случаи в семье пользования кредитом', 3);
INSERT INTO `ki_questions` VALUES(2, 'У вас была просрочка покредиту', 1, 'Была ли ситуация, когда вы не смогли вовремя внести платеж по кредиту', 3);
INSERT INTO `ki_questions` VALUES(3, 'Сколько раз брали кредит', 2, 'Количество кредитов', 2);
INSERT INTO `ki_questions` VALUES(4, 'Укажите девичью фамилию матери', 3, 'Хинт 4', 1);
INSERT INTO `ki_questions` VALUES(5, 'Дата последнего кредита', 5, 'Укажите когда вы в последний разбрали кредит', 4);
INSERT INTO `ki_questions` VALUES(6, 'Максимально просрочили дней', 0, 'Сколько дней просрочки по платежу максимально у вас было', 5);
INSERT INTO `ki_questions` VALUES(7, 'В какой валюте лучше всего брать кредиты', 6, 'Как вы считаете, в какой валюте лучше всего брать кредиты', 2);
INSERT INTO `ki_questions` VALUES(8, 'Сумма кредита', 7, 'На какую сумму кредита вы рассчитываете', 1);
INSERT INTO `ki_questions` VALUES(9, 'Чем ответите', 7, 'Чем отвечает заемщик по кредитным обязательствам?', 1);
INSERT INTO `ki_questions` VALUES(10, 'Досрочное погашение', 8, 'Можно ли досрочно погасить потребительский кредит', 3);

-- --------------------------------------------------------

--
-- Table structure for table `ki_questions_variants`
--

DROP TABLE IF EXISTS `ki_questions_variants`;
CREATE TABLE `ki_questions_variants` (
  `id` int(11) NOT NULL,
  `id_question` int(11) NOT NULL COMMENT 'Ссылка на вопрос',
  `variant` varchar(200) NOT NULL DEFAULT '' COMMENT 'Вариант ответа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список вариантов ответа, если select';

--
-- Dumping data for table `ki_questions_variants`
--

INSERT INTO `ki_questions_variants` VALUES(1, 3, '1-10');
INSERT INTO `ki_questions_variants` VALUES(2, 3, '11-100');
INSERT INTO `ki_questions_variants` VALUES(3, 7, 'Доллары');
INSERT INTO `ki_questions_variants` VALUES(4, 7, 'Гривны');
INSERT INTO `ki_questions_variants` VALUES(5, 7, 'Рубли');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ki_answers`
--
ALTER TABLE `ki_answers`
  ADD PRIMARY KEY (`ki_answer_key`),
  ADD KEY `ki_question_key` (`ki_question_key`),
  ADD KEY `application_key` (`application_key`);

--
-- Indexes for table `ki_answers_formats`
--
ALTER TABLE `ki_answers_formats`
  ADD PRIMARY KEY (`ki_answer_format_key`);

--
-- Indexes for table `ki_applications`
--
ALTER TABLE `ki_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ki_questions`
--
ALTER TABLE `ki_questions`
  ADD PRIMARY KEY (`ki_question_key`),
  ADD KEY `answer_format_key` (`answer_format_key`);

--
-- Indexes for table `ki_questions_variants`
--
ALTER TABLE `ki_questions_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_question` (`id_question`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ki_answers`
--
ALTER TABLE `ki_answers`
  MODIFY `ki_answer_key` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `ki_answers_formats`
--
ALTER TABLE `ki_answers_formats`
  MODIFY `ki_answer_format_key` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ki_applications`
--
ALTER TABLE `ki_applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ki_questions`
--
ALTER TABLE `ki_questions`
  MODIFY `ki_question_key` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `ki_questions_variants`
--
ALTER TABLE `ki_questions_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ki_answers`
--
ALTER TABLE `ki_answers`
  ADD CONSTRAINT `ki_answers_ibfk_1` FOREIGN KEY (`application_key`) REFERENCES `ki_applications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ki_answers_ibfk_2` FOREIGN KEY (`ki_question_key`) REFERENCES `ki_questions` (`ki_question_key`);

--
-- Constraints for table `ki_questions`
--
ALTER TABLE `ki_questions`
  ADD CONSTRAINT `ki_questions_ibfk_1` FOREIGN KEY (`answer_format_key`) REFERENCES `ki_answers_formats` (`ki_answer_format_key`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ki_questions_variants`
--
ALTER TABLE `ki_questions_variants`
  ADD CONSTRAINT `ki_questions_variants_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `ki_questions` (`ki_question_key`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
