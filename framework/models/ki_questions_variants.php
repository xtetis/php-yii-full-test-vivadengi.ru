<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ki_questions_variants".
 *
 * @property integer $id
 * @property integer $id_question
 * @property string $variant
 *
 * @property KiQuestions $idQuestion
 */
class ki_questions_variants extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ki_questions_variants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_question'], 'required'],
            [['id_question'], 'integer'],
            [['variant'], 'string', 'max' => 200],
            [['id_question'], 'exist', 'skipOnError' => true, 'targetClass' => KiQuestions::className(), 'targetAttribute' => ['id_question' => 'ki_question_key']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_question' => 'Id Question',
            'variant' => 'Variant',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQuestion()
    {
        return $this->hasOne(KiQuestions::className(), ['ki_question_key' => 'id_question']);
    }
}
