<?php

namespace app\models;

use Yii;
use app\models\ki_applications;
use app\models\ki_questions;



/**
 * This is the model class for table "ki_answers".
 *
 * @property integer $ki_answer_key
 * @property integer $application_key
 * @property integer $ki_question_key
 * @property string $answer
 *
 * @property KiApplications $applicationKey
 * @property KiQuestions $kiQuestionKey
 */
class ki_answers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ki_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_key', 'ki_question_key'], 'required'],
            [['application_key', 'ki_question_key'], 'integer'],
            [['answer'], 'string', 'max' => 200],
            [['application_key'], 'exist', 'skipOnError' => true, 'targetClass' => ki_applications::className(), 'targetAttribute' => ['application_key' => 'id']],
            [['ki_question_key'], 'exist', 'skipOnError' => true, 'targetClass' => ki_questions::className(), 'targetAttribute' => ['ki_question_key' => 'ki_question_key']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ki_answer_key' => 'Ki Answer Key',
            'application_key' => 'Application Key',
            'ki_question_key' => 'Ki Question Key',
            'answer' => 'Answer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplicationKey()
    {
        return $this->hasOne(ki_applications::className(), ['id' => 'application_key']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKiQuestionKey()
    {
        return $this->hasOne(ki_questions::className(), ['ki_question_key' => 'ki_question_key']);
    }
}
