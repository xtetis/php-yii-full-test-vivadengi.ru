<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ki_applications".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 *
 * @property KiAnswers[] $kiAnswers
 */
class ki_applications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ki_applications';
    }

    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
				[['phone','first_name', 'last_name'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
				[['phone', 'first_name', 'last_name','use_credit'], 'trim'],
				[['first_name'], 'string', 'encoding'=>'UTF-8', 'min' => 3, 'tooShort'=>'Поле "{attribute}" не может быть короче 3 символов'],
				[['last_name'], 'string', 'encoding'=>'UTF-8', 'min' => 2, 'tooShort'=>'Поле "{attribute}" не может быть короче 2 символов'],
				['phone', 'match', 'pattern' => '/^[0-9]{6,12}$/i','message'=>'Поле "Телефон" может содержать только цифры (от 6 до 12 символов)'],
				[['phone'], 'string', 'encoding'=>'UTF-8', 'min' => 6, 'tooShort'=>'Поле "{attribute}" не может быть короче 6 символов'],
				['phone', 'unique', 'message'=>'Этот телефон уже зарегистрирован'],
			];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'phone' => 'Телефон',
            'questions' => 'Список сгенерированных вопросов',
            'use_credit' => 'Пользовались ли ранее кредитом',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKiAnswers()
    {
        return $this->hasMany(KiAnswers::className(), ['application_key' => 'id']);
    }
}
