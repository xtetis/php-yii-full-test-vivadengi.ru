<?php

namespace app\models;

use Yii;
use app\models\ki_answers_formats;
use app\models\ki_answers;
use app\models\ki_applications;
use app\models\ki_questions_variants;




/**
 * This is the model class for table "ki_questions".
 *
 * @property integer $ki_question_key
 * @property string $question_text
 * @property integer $question_group
 * @property string $hint_text
 * @property integer $answer_format_key
 *
 * @property KiAnswers[] $kiAnswers
 * @property KiAnswersFormats $answerFormatKey
 * @property KiQuestionsVariants[] $kiQuestionsVariants
 */
class ki_questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ki_questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_group', 'answer_format_key'], 'integer'],
            [['question_text', 'hint_text'], 'string', 'max' => 200],
            [['answer_format_key'], 'exist', 'skipOnError' => true, 'targetClass' => KiAnswersFormats::className(), 'targetAttribute' => ['answer_format_key' => 'ki_answer_format_key']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ki_question_key' => 'Ki Question Key',
            'question_text' => 'Question Text',
            'question_group' => 'Question Group',
            'hint_text' => 'Hint Text',
            'answer_format_key' => 'Answer Format Key',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKiAnswers()
    {
        return $this->hasMany(ki_answers::className(), ['ki_question_key' => 'ki_question_key']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerFormatKey()
    {
        return $this->hasOne(ki_answers_formats::className(), ['ki_answer_format_key' => 'answer_format_key']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKiQuestionsVariants()
    {
        return $this->hasMany(ki_questions_variants::className(), ['id_question' => 'ki_question_key']);
    }
}
