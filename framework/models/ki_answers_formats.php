<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ki_answers_formats".
 *
 * @property integer $ki_answer_format_key
 * @property string $answer_format_description
 *
 * @property KiQuestions[] $kiQuestions
 */
class ki_answers_formats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ki_answers_formats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answer_format_description'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ki_answer_format_key' => 'Ki Answer Format Key',
            'answer_format_description' => 'Answer Format Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKiQuestions()
    {
        return $this->hasMany(KiQuestions::className(), ['answer_format_key' => 'ki_answer_format_key']);
    }
}
