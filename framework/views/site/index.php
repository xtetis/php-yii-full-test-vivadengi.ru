<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\ki_answers_formats;
use app\models\ki_answers;
use app\models\ki_applications;
use app\models\ki_questions;
use app\models\ki_questions_variants;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
	<div class="body-content">
		<div class="row">
			<div class="col-md-2">
				<ul class="nav nav-pills nav-stacked" role="tablist">
					<li role="presentation" <?if (!$model_ki_applications->id):?> class="active" <?endif;?>>
						<a href="#user" aria-controls="user" role="tab" data-toggle="tab">
							<div class="text-center" style=""><strong>Шаг 1</strong></div>
							<div class="text-center" style="font-size:12px;">Представьтесь</div>
						</a>
					</li>
					<?if ($model_ki_applications->id):?>
					<li role="presentation" class="active">
						<a href="#anketa" aria-controls="anketa" role="tab" data-toggle="tab">
							<div class="text-center" style=""><strong>Шаг 2</strong></div>
							<div class="text-center" style="font-size:12px;">Анкетирование</div>
						</a>
					</li>
					<?endif;?>
				</ul>
			</div>
			<div class="col-md-10">
<div class="panel panel-default">
	<div class="panel-body">			
				<div class="tab-content">

					<div role="tabpanel" class="tab-pane <?if (!$model_ki_applications->id):?>active<?endif;?>" id="user">
						<?php $form = ActiveForm::begin(['id' => 'register-form']); ?>






<?=$form->field($model_ki_applications, 'first_name',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <b style="color:#f00;">*</b>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Укажите имя" 
       data-content="Напишите ваше имя. Минимум 3 симввола.">
     {input} 
     <span class="red rederror"></span>
     {error}
  </div>
</div>'
,'errorOptions' => ['encode' => false]])->textInput() ?>





<?=$form->field($model_ki_applications, 'last_name',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <b style="color:#f00;">*</b>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Укажите фамилию" 
       data-content="Напишите вашу фамилию. Минимум 2 симввола.">
     {input}
     <span class="red rederror"></span>
     {error}
  </div>
</div>'
,'errorOptions' => ['encode' => false]])->textInput() ?>






<?
$inputOptions = ['class' => 'form-control intval'];
if($model_ki_applications->id)
{
	$inputOptions = ['readonly'=>'readonly'] + $inputOptions;
}
echo $form->field($model_ki_applications, 'phone',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <b style="color:#f00;">*</b>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Укажите телефон" 
       data-content="Укажите контактный номер телефона">
     {input}
     <span class="red rederror"></span>
     {error}
  </div>
</div>'
,'errorOptions' => ['encode' => false],
'inputOptions'=>$inputOptions])->textInput() ?>





<?=$form->field($model_ki_applications, 'use_credit',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Использование кредита" 
       data-content="Укажите, пользовалили выранее кредитом">
     {input}
     <span class="red rederror"></span>
     {error}
  </div>
</div>'
,'errorOptions' => ['encode' => false]])->checkBox(['value' => '1','checked'=>$model_ki_applications->use_credit]) ?>








				<div class="form-group text-center" style="margin-top:50px;">
				
						<?= Html::submitButton(((intval($model_ki_applications->id))?'Сохранить изменения':'Сохранить'), ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
				</div>
		
						<?php ActiveForm::end(); ?>
				

					</div>
					<div role="tabpanel" class="tab-pane <?if ($model_ki_applications->id):?>active<?endif;?>" id="anketa">
						<?php $form1 = ActiveForm::begin(['id' => 'question-form']); ?>
						
						<?foreach ($model__ki_questions_array as $model__ki_questions):
						
						
$answerFormatKey = $model__ki_questions->getAnswerFormatKey()->one()->answer_format_description;
$answer_name = 'answer__'.$model__ki_answers_array[$model__ki_questions->ki_question_key]->ki_answer_key;
$answer_value = $model__ki_answers_array[$model__ki_questions->ki_question_key]->answer;

$requided = array();
if($model_ki_applications->use_credit)
{
	$requided = ['required'=>'required'];
}


if ($answerFormatKey=='text'):
?>

<div class="row" style="margin-bottom:10px;">
  <div class="col-md-12" >
    <label class="control-label">
<?=$model__ki_questions->question_text?>
    </label>
    <b style="color:#f00;">*</b>
  </div>
  <div class="col-md-9" 
       data-toggle="popover" 
       data-title="Заполните поле" 
       data-content="<?=htmlspecialchars($model__ki_questions->hint_text)?>">
     <?=Html::textInput($answer_name, $answer_value, ['class'=>"form-control"]+$requided)?> 
  </div>
</div>
<?
elseif($answerFormatKey=='date'):
?>
<div class="row" style="margin-bottom:10px;">
  <div class="col-md-12" >
    <label class="control-label">
<?=$model__ki_questions->question_text?>
    </label>
    <b style="color:#f00;">*</b>
  </div>
  <div class="col-md-9" 
       data-toggle="popover" 
       data-title="Заполните поле" 
       data-content="<?=htmlspecialchars($model__ki_questions->hint_text)?>">
     <?=Html::textInput($answer_name, $answer_value, ['class'=>"form-control readonly" ,'datepicker'=>'datepicker']+$requided)?> 
  </div>
</div>

<?

elseif($answerFormatKey=='int'):
?>
<div class="row" style="margin-bottom:10px;">
  <div class="col-md-12" >
    <label class="control-label">
<?=$model__ki_questions->question_text?>
    </label>
    <b style="color:#f00;">*</b>
  </div>
  <div class="col-md-9" 
       data-toggle="popover" 
       data-title="Заполните поле" 
       data-content="<?=htmlspecialchars($model__ki_questions->hint_text)?>">
     <?=Html::textInput($answer_name, $answer_value, ['class'=>"form-control intval"]+$requided)?> 
  </div>
</div>

<?

elseif($answerFormatKey=='checkbox'):
?>
<div class="row" style="margin-bottom:10px;">
  <div class="col-md-9" data-toggle="popover" 
       data-title="Напишите заголовок" 
       data-content="<?=htmlspecialchars($model__ki_questions->hint_text)?>">
    <label class="control-label">
			<?=$model__ki_questions->question_text?>
       
     <?=Html::checkbox($answer_name, intval($answer_value), ['value'=>'1'])?> 
     </label>
  </div>
</div>

<?
elseif($answerFormatKey=='select'):

$variants = $model__ki_questions->getKiQuestionsVariants()->all();
$options = array();
foreach ($variants as $variant_item)
{
	$options[$variant_item->variant]=$variant_item->variant;
}

$options = [''=>'Выберите ответ','NULL'=>'Не было кредитов'] + $options;

?>


<div class="row" style="margin-bottom:10px;">
  <div class="col-md-12" >
    <label class="control-label">
<?=$model__ki_questions->question_text?>
    </label>
    <b style="color:#f00;">*</b>
  </div>
  <div class="col-md-9" 
       data-toggle="popover" 
       data-title="Напишите заголовок" 
       data-content="<?=htmlspecialchars($model__ki_questions->hint_text)?>">
     <?=Html::listBox($answer_name,$answer_value, $options,
['text' => 'Please select', 'options' => ['value' => 'none', 'label' => 'Select'],'size'=>1,'class' => 'form-control','onchange'=>'$("select[name=\''.$answer_name.'\'] option[value=\'\']").attr(\'disabled\',\'disabled\');']+$requided)?> 
  </div>
</div>


<?
endif;
 ?>



						<?endforeach;?>

						<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'questions']) ?>
						
						<?php ActiveForm::end(); ?>
					</div>

				</div>
					
	</div>
</div>
			</div>
		</div>
	</div>
</div>
