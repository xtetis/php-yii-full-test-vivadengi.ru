<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
//use app\assets\AppAsset;

//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
        <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script-->

<link href="/css/site.css" rel="stylesheet">
<link href="/themes/default/components/bdatepicker/css/bootstrap-datepicker.css" rel="stylesheet">


<?
$js = '
    $("[data-toggle=\'tooltip\']").tooltip();
    $("[data-toggle=\'popover\']").popover({ trigger : \'hover focus\',html: true});
    
    $("[datepicker=\'datepicker\']").datepicker({
    format: \'dd.mm.yyyy\',
    startDate: \'-1d\'});
    
    
    
    
    
// Проверка на ввод целочисленных данных
//================================================
$(".intval").keydown(function(event) {
    // Allow: backspace, delete, tab, escape, enter and .
    // "-" has a charCode of 45.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) || 
         // Allow: Ctrl+V
        (event.keyCode == 86 && event.ctrlKey === true) || 
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
             // let it happen, dont do anything
             return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (
             event.shiftKey || 
             (event.keyCode < 48 || event.keyCode > 57) && 
             (event.keyCode < 96 || event.keyCode > 105 ) //&& (event.keyCode != 173 )
           ) {
               event.preventDefault(); 
             }
    }
});
//================================================


    $(".readonly").on("keydown paste", function(e){
        e.preventDefault();
    });

';


$this->registerJs($js);
?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    /*
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    */
    ?>

    <div class="container">
        <?
        /*
        = Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])*/ ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Тестовая задача <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/themes/default/components/bootstrap-3.3.7-dist/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="/themes/default/components/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="/themes/default/components/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script src="/themes/default/components/bdatepicker/js/bootstrap-datepicker.min.js"></script>


</body>
</html>
<?php $this->endPage() ?>
