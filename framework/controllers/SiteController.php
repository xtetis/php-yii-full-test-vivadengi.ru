<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


use app\models\ki_answers_formats;
use app\models\ki_answers;
use app\models\ki_applications;
use app\models\ki_questions;
use app\models\ki_questions_variants;

use  yii\db\Query;




class SiteController extends Controller
{

	public function actionIndex()
	{
		$data = array();


		if (
		     (isset($_COOKIE['appid'])) &&
		     (isset($_COOKIE['appkey']))
		   )
		{
			$model_ki_applications = ki_applications::findOne(intval($_COOKIE['appid']));
			if (
			     (!$model_ki_applications) ||
			     (md5($model_ki_applications->phone)!=$_COOKIE['appkey'])
			   )
			{
				$model_ki_applications = new ki_applications();
			}
		}
		else
		{
			$model_ki_applications = new ki_applications();
		}
		
		
		
		$model__ki_questions_array = array();
		$model__ki_answers_array = array();
		if ($model_ki_applications->id)
		{
			if ($model_ki_applications->questions=='')
			{
				$query = new Query;
				$query->select(['question_group']);
				$query->distinct();
				$query->from('ki_questions');
				$command = $query->createCommand();
				$select = $command->queryAll();
				$q_ids = array();
				foreach ($select as $row)
				{
					$model__ki_questions = ki_questions::find()->where(["question_group" => $row['question_group']])->orderBy('RAND()')->limit(1)->one();
					if ($model__ki_questions)
					{
						$model__ki_questions_array[]=$model__ki_questions;
						$q_ids[]= $model__ki_questions->ki_question_key;
					}
				}
				
				if (count($q_ids))
				{
				 $model_ki_applications->questions=implode(',',$q_ids);
				 $model_ki_applications->save();
				}
				
				
			}
			else
			{
				$questions_id_array = explode(',',$model_ki_applications->questions);
				foreach ($questions_id_array as $questions_id_array_item)
				{
					$model__ki_questions = ki_questions::findOne(intval($questions_id_array_item));
					if ($model__ki_questions)
					{
						$model__ki_questions_array[]=$model__ki_questions;
					}
				}
				
			}
			
			foreach ($model__ki_questions_array as $model__ki_questions)
			{
				$model__ki_answers = ki_answers::find()->where(["application_key" => $model_ki_applications->id,"ki_question_key"=>$model__ki_questions->ki_question_key])->orderBy('RAND()')->limit(1)->one();
				
				if(!$model__ki_answers)
				{
					$model__ki_answers = new ki_answers();
					$model__ki_answers->ki_question_key = $model__ki_questions->ki_question_key;
					$model__ki_answers->application_key = $model_ki_applications->id;
					$model__ki_answers->answer='';
					$model__ki_answers->save();
				}
				
				if ($model__ki_answers)
				{
					$model__ki_answers_array[$model__ki_questions->ki_question_key] = $model__ki_answers;
				}
			}
			

		}
		
		
		if (isset($_POST['questions']))
		{
			foreach ($model__ki_questions_array as $model__ki_questions)
			{
				if (
				     (isset($model__ki_answers_array[$model__ki_questions->ki_question_key]))
				   )
				{
					$answer_format_description = $model__ki_questions->getAnswerFormatKey()->one()->answer_format_description;
					
					
					
					if (isset($_POST['answer__'.$model__ki_answers_array[$model__ki_questions->ki_question_key]->ki_answer_key]))
					{
					
						$post_val = $_POST['answer__'.$model__ki_answers_array[$model__ki_questions->ki_question_key]->ki_answer_key];
						if($answer_format_description=='int')
						{
							$post_val = preg_replace("/\D/","",$post_val);
						}
						
						$model__ki_answers_array[$model__ki_questions->ki_question_key]->answer = $post_val;
						$model__ki_answers_array[$model__ki_questions->ki_question_key]->save();
					}
					elseif($answer_format_description =='checkbox')
					{
						$model__ki_answers_array[$model__ki_questions->ki_question_key]->answer = '';
						$model__ki_answers_array[$model__ki_questions->ki_question_key]->save();
					}
					
				}
			}
		}
		
		
		
		if (
			   $model_ki_applications->load(Yii::$app->request->post()) && 
			   $model_ki_applications->validate()
			 )
		{
			
			$model_ki_applications->save();
			//print_r($_POST); exit;
			setcookie("appid", $model_ki_applications->id, time()+604800, "/");
			setcookie("appkey", md5($model_ki_applications->phone), time()+604800, "/");
			header("Location: /");
			exit();
		}
		

		$data['model_ki_applications'] = $model_ki_applications;
		$data['model__ki_questions_array'] = $model__ki_questions_array;
		$data['model__ki_answers_array'] = $model__ki_answers_array;
		return $this->render('index',$data);
	}


}
